package baca.brian.ui;
import baca.brian.bl.*;
import baca.brian.bl.logic.Gestor;
import baca.brian.bl.logic.Gestor.*;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Random;
import baca.brian.bl.entities.Subasta.*;
import baca.brian.bl.entities.Usuario.*;


public class UI {
    static BL bl = new BL();
    static Gestor gt = new Gestor();
    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;
    public static void main(String[] args) throws IOException{
        // write your code here
        int opcion= -1;
        inicializarPrograma();//esto es opcional
        do{
            mostrarMenu();
            opcion = seleccionarOpcion();
            procesarOpcion(opcion);
        }while (opcion!=0);

    }
    static void mostrarMenu(){
        out.println("1) Registrarse como Administrador");
        out.println("2) Registrarse como Vendedor");
        out.println("3) Registrarse como Coleccionista");
        out.println("4) Acceder al sistema");
        out.println("5) Observar Subastas");
        out.println("0) Salir");
    }

    static void procesarOpcion(int pOpcion) throws IOException {
        switch (pOpcion){
            case 1:
                crearUsuarioAdministrador();
                break;
            case 2:
                crearUsuarioVendedor();
                break;
            case 3:
                crearUsuarioColeccionista();
                break;
            case 4:
                accederSitema();
                break;
            case 5:
                mostrarSubastas();
                break;
            case 0:
                out.println("Gracias por usar el programa");
                break;
            default:
                out.println("Opción inválida");
                break;
        }
    }
    static void inicializarPrograma()throws IOException{

    }

    static int seleccionarOpcion() throws IOException{
        out.println("Digite la opcion");
        return Integer.parseInt(in.readLine());
    }

    public static void crearUsuarioAdministrador() throws IOException{
        out.println("Ingrese su nombre: ");
        String nombre = in.readLine();

        out.println("Ingrese su apellido: ");
        String apellido = in.readLine();

        out.println("Ingrese su id: ");
        String id = in.readLine();

        out.println("Ingrese su contrasena: ");
        String contrasena = in.readLine();

        out.println("Digite el día de la fecha de nacimiento");
        int dia = Integer.parseInt(in.readLine());
        out.println("Digite el mes de la fecha de nacimiento");
        int mes = Integer.parseInt(in.readLine());
        out.println("Digite el año de la fecha de nacimiento");
        int year = Integer.parseInt(in.readLine());

        out.println("Digite su edad: ");
        int edad = Integer.parseInt(in.readLine());

        out.println("Digite su estado: Activo/Inactivo");
        String estado = in.readLine();

        out.println("Digite su email: ");
        String email = in.readLine();

        out.println("Digite su direccion: ");
        String direccion = in.readLine();

        int tipo = 2;

        gt.registrarAdministradorr(nombre,apellido,id,contrasena,dia,mes,year,edad,estado,email,tipo);
    }

    public static void crearUsuarioVendedor() throws IOException{

        out.println("Ingrese su nombre: ");
        String nombre = in.readLine();

        out.println("Ingrese su direccion: ");
        String direccion = in.readLine();

        out.println("Ingrese su correo electronico: ");
        String email = in.readLine();

        int tipo = 0;

        String msj = bl.registrarVendedor(nombre,direccion,email,tipo);
        out.println(msj);
    }

    public static void crearUsuarioColeccionista() throws IOException{
        out.println("Ingrese su nombre: ");
        String nombre = in.readLine();

        out.println("Ingrese su apellido: ");
        String apellido = in.readLine();

        out.println("Ingrese su id: ");
        String id = in.readLine();

        out.println("Ingrese su contrasena: ");
        String contrasena = in.readLine();

        out.println("Digite el día de la fecha de nacimiento");
        int dia = Integer.parseInt(in.readLine());
        out.println("Digite el mes de la fecha de nacimiento");
        int mes = Integer.parseInt(in.readLine());
        out.println("Digite el año de la fecha de nacimiento");
        int year = Integer.parseInt(in.readLine());

        out.println("Digite su edad: ");
        int edad = Integer.parseInt(in.readLine());

        out.println("Digite su estado: Activo/Inactivo");
        String estado = in.readLine();

        out.println("Digite su email: ");
        String email = in.readLine();

        out.println("Ingrese su direccion: ");
        String direccion = in.readLine();

        out.println("Digite su puntuacion: ");
        String puntuacion = in.readLine();

        int tipo = 1;

        String msj = bl.registrarColeccionista(nombre,apellido,id,contrasena,dia,mes,year,edad,estado,email,puntuacion,tipo,direccion);
        out.println(msj);
    }

    public static void accederSitema() throws  IOException{
        out.println("Ingrese su nombre: ");
        String nombre = in.readLine();

        out.println("Ingrese su email: ");
        String email = in.readLine();

        int tipo = bl.buscarUsuario(nombre,email);
        out.println("tipo: "+tipo);
        //si retorna 0 es vendedor, 1 comprador, 2 administrador
        if(tipo==0)
        {
            menuVendedor(nombre,email);
        }
        else if(tipo==1)
        {
            menuComprador(nombre,email);
        }
        else if(tipo==2)
        {
            menuAdministrador(nombre,email);
        }
        else if(tipo!=0 && tipo!=2)
        {
            out.println("Email o nombre incorrecto.");
        }
    }

    public static void subastando(String nombre, String email) throws IOException{
        Random r = new Random();
        int codigo = r.nextInt(99999999);

        out.println("Ingrese el nombre del producto: ");
        String nombreArticulo = in.readLine();

        out.println("Ingrese descripción del producto: ");
        String descripcion = in.readLine();

        out.println("Ingrese el estado del producto: ");
        String estado = in.readLine();

        out.println("Digite el día de la fecha de antiguedad del producto");
        int dia = Integer.parseInt(in.readLine());
        out.println("Digite el mes de la fecha de antiguedad del producto");
        int mes = Integer.parseInt(in.readLine());
        out.println("Digite el año de la fecha de antiguedad del producto");
        int year = Integer.parseInt(in.readLine());

        out.println("Valor del artículo: ");
        int valor = Integer.parseInt(in.readLine());

        out.println("Ingrese la categoria de su articulo: ");
        String categoria = in.readLine();

        out.println("Ingrese el ID de su articulo: ");
        int id = Integer.parseInt(in.readLine());


        String fechainicio = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());


        String msj = bl.registrarArticulo(codigo,nombreArticulo,descripcion,estado,dia,mes,year,valor,categoria,fechainicio,nombre,email,id);
        out.println(msj);

    }



    public static void menuComprador(String nombre, String email)throws IOException{
        //mostrar menu de comprador con while hasta que quiera salir!
        //comprador puede pujar!!
        int op=1;
        while(op!=0)
        {
            out.println("1) Listar Subastas ");
            out.println("2) Pujar Subasta");
            out.println("3) Listar Pujas Realizadas");
            out.println("0) Salir");

            out.println("Ingrese opción: ");
            op = Integer.parseInt(in.readLine());

            if(op==1)
            {
                imprimirSubastas();
            }

            else if(op==2)
            {
                out.println("Ingrese el código de la subasta: ");
                int codigosubasta = Integer.parseInt(in.readLine());

                out.println("Ingrese el valor de la puja: ");
                int valorpuja = Integer.parseInt(in.readLine());

                out.println("Ingrese el id de la puja: ");
                int idPuja = Integer.parseInt(in.readLine());

                Puja p = new Puja(nombre, valorpuja, codigosubasta,idPuja);

                boolean fueAgregada = bl.agregarPujaSubasta(codigosubasta, p);

                if(fueAgregada==true)
                {
                    bl.agregarPujaUsuario(nombre, email, p);
                    out.println("Puja añadida con éxito.");
                }
                else
                {
                    out.println("Subasta no encontrada.");
                }

            }

            else if(op==3)
            {
                listarPujasUsuario(email);
            }
        }
    }

    public static void menuVendedor(String nombre, String email) throws IOException{
        int op=1;
        //mostrar menu de vendedor con while hasta que quiera salir!
        //vendedor puede modificar artículos
        while(op!=0)
        {
            out.println("1) Crear Subasta ");
            out.println("2) Mostrar Subastas");
            out.println("0) Salir");

            out.println("Ingrese opción: ");
            op = Integer.parseInt(in.readLine());

            if(op==1)
            {
                subastando(nombre,email);
            }

            else if(op==2)
            {
                imprimirSubastas();
            }
        }
    }

    public static void mostrarSubastas() throws IOException{
        out.println("1) Mostrar Subastas ");
        out.println("2) Volver al inicio");
        out.println("0) Salir");

        out.println("Ingrese Opción: ");
        int op = Integer.parseInt(in.readLine());

        if(op==1)
        {
            imprimirSubastas();
        }
    }

    public static void imprimirSubastas()
    {
        for (Iterator<Subasta> it = bl.subastas.iterator(); it.hasNext();)
        {
            Subasta s = it.next();
            out.println("Código: "+s.getCodigosubasta());
            out.println(s.getArticulo().toString());
        }
    }


    public static void listarPujasUsuario(String email)
    {

        for (Iterator<Usuario> it = bl.usuarios.iterator(); it.hasNext();)
        {
            Usuario u = it.next();
            if(u.getEmail().equals(email))
            {
                ArrayList<Puja> pujas = u.getPujas();
                int i=0;
                if(pujas!=null)
                {
                    for (Iterator<Puja> it2 = pujas.iterator(); it2.hasNext();)
                    {
                        Puja p = it2.next();
                        out.println(i+")"+" Código Subasta: "+p.getCodigosubasta());
                        out.println("Valor: "+p.getValorpuja());
                        i+=1;
                    }
                }
            }
        }
    }

    public static void menuAdministrador(String nombre, String email) throws IOException{
        int op=1;
        while(op!=0)
        {
            out.println("1) Eliminar subasta");
            out.println("0) Salir");

            out.println("Ingrese opción: ");
            op = Integer.parseInt(in.readLine());

            if(op==1)
            {
                out.println("Ingrese el código de la subasta: ");
                int codigosubasta = Integer.parseInt(in.readLine());

                boolean fueAgregada = bl.eliminarSubasta(codigosubasta);

                if(fueAgregada==true)
                {
                    bl.eliminarSubasta(codigosubasta);
                    out.println("Eliminada con exito");
                }
                else
                {
                    out.println("Subasta no encontrada.");
                }
            }

        }
    }


}